# Projeto Fonte de Tensão Ajustável entre 3V a 12V com capacidade de 100mA

## Introdução
Nesse projeto iremos construir uma fonte de tensão ajustável entre 3V e 12V com capacidade de 100mA. Será contemplado desde a esquematização do circuito nas ferramentas Falstad e EAGLE até a montagem da fonte com os componentes selecionados.

## Alunos

| Nome        | Número USP     |
| ------------- |:-------------:|
| Marcus Vinícius Teixeira Huziwara     | 11834432 | 
|   Antônio Rodrigues Rigolino   |    11795791   | 
| Enzo Key Yamashita |  13678630  | 
| Alessandro Rodrigues Pereira da Silva |  15436838  | 

## Disciplina

SSC0180 - Eletrônica para Computação

##  Professor

Eduardo do Valle Simões

## Lista dos Componentes

| Quantidade    | Nome     | Custo da unidade |
| ------------- |:-------------:|:-------------:|
| 1 | Transformador | Fornecido pelo professor | 
| 1 | Potenciometro 5kΩ linear | R$ 2.00 | 
| 1 | Transistor Bipolar NPN | R$ 0.19 | 
| 4 | Diodo 1N4007 | R$ 0.24 | 
| 1 | Capacitor 680uF x 50V | R$ 0.95 | 
| 1 | LED | R$ 0.20 | 
| 3 | Resistor 2.2kΩ 1W 5% | R$ 0.10 | 
| 1 | Resistor 100Ω 2W 5% | R$ 0.10 | 
| 1 | Diodo Zener 13V 1W | R$ 0.25 | 
| 1 | Protoboard  | R$ 11.90 |
| 1 | Kit Jumpers | R$ 15.00 |  

## Descrição dos Componentes

#### Transformador

Componente responsável por diminuir a tensão de 127V vinda da fonte de corrente alternada (tomada). Dado um pico de tensão de 180V.

#### Ponte Retificadora

É formada por 4 diodos que permitem a passagem da corrente elétrica em apenas uma direção, será utilizada para converter tensão AC em tensão DC. Considerenado uma perda 1.4V, 0.7V para cada diodo no percurso da corrente.

#### Capacitor

Componente eletrônico passivo que armazena energia elétrica, liberando corrente quando a tensão interna é maior que a tensão vinda da fonte. Servirá como capacitor de filtragem, suavisando as variações de tensão. Além disso, o modelo do capacitor foi escolhido visando atingir um ripple de 10%.

#### Diodo Zener

É o responsável por regular da tensão máxima, com uma tensão de ruptura de 13V, ajudando a eliminar o ripple do circuito. Ou seja, ele servirá para cortar a tensão de saída do capacitor abaixo da oscilação. 

#### Potênciometro

Irá regular a tensão fornecida pela fonte entre 12V e 3V. 

#### Transistor NPN

Servirá para regular a tensão emitida juntamente com o Zener, limitando a saída para 12V.

#### Resistores

Haverão 4 resistores em nosso circuito, um será responsável por controlar a corrente passada pelo o LED, enquanto a outra também terá um papel similar limitando a corrente vinda ao Zener (evitando superaquecimento). Depois adicionaremos uma terceira resistencia, junto ao potenciometro, para regular a tensào mínima em 3V. O resistor do Zener foi escolhido visando manter uma baixa corrente passando pelo Zener, abaixo de 5 mA, evitando um sobreaquecimento. Também foi considerado que o valor da resistência não fosse o suficiente para gerar uma queda de tensão abaixo da tensão de ruptura do Zener. Por fim, haverá uma resistência de 100 para controlar a corrente que chega no coletor do transistor, evitando a queima do componente. Devido à alta corrente e tensão passada chegada nessa resistência, ela deverá ter uma potência de 2W.

Obs: o resistor de 100 não foi adicionado na projeto, devido ao fato do grupo não ter encontrado a resistência com essas especificações.

#### LED

Adicionado para verificar o funcionamento do circuito. (Ornamental)

## Cálculos

#### Fonte

RMS = 127V

Tensão de Pico = 127V *  $`\sqrt{2}`$ = 179,6V = 180V 

#### Transformador e Diodos

Foi escolhido o transformador com voltagem AC de 18.1V e 24.2V no capacitor do circuito demonstrado pelo professor. Para definir a razão de espiras no circuito do falstad, considerando a perda de voltagem de 0.7V para cada diodo, foram testadas no simulador diferentes razões e a que mais se aproximou do valor de 24.2V no capacitor foi a de 7.1.

P1 = P2

V1 * A1 = V2 * A2

N1/N2 = 7.1

V1 = 180V

N1/N2 = 180V/V2

V2 = 180V/7.1

V2 = 25,35V

V = 25,35V - (2 * 0.7V) = 23,95V

#### Capacitor

Para definir a capacitância mínima do capacitor a ser utilizado no nosso circuito, será considerado um ripple de 10% e calculada a capacitância com a equação simplificada do ripple.

Vripple = I / 2 * f * C

ripple de 10% 

Vripple = 0.1 * 23,95V = 2.395V

Capacitância mínima = 128/(2 * 60Hz * 2.395V) = 445.37 uF

#### Zener e Transistor

Vb = 13V (tensão vindo do Zener para a base)

Vc = 23.95V (tensão vindo do capacitor para o coletor)

Perda de 0.7V no emissor do transistor

Ve = 13V - 0.7V = 12.3V (tensão de saída no emissor)

#### Correntes

**corrente de saída = 101mA**

**corrente LED**

Voltage vinda do capacitor = 23,95V

Voltagem gasta pelo LED = 1.8V

resistência do LED = 2.2K

corrente LED = 23,95V - 1.8V / 2.2K = 10mA

Potência no LED = 10mA * 1.8V = 18.1mW

**corrente Zener e Potênciometro**

Voltagem chegada no Zener = 23,95V

Vzener = 13V

R = 2K ohms

Vr = 23,95V-13V = 10.95V

corrente passada para o zener e o potenciometro = 10.95V/2K = 5.4mA

corrente potenciometro = 13V/5k = 2.60mA

corrente zener = 5.4mA - 2.6mA = 2.8mA

potencia zener = 13V * 2.8mA = 36.4mW

corrente de saida na carga = 101mA

**corrente total = 14.8mA + 5.4mA + 101mA = 121.2mA**

## Circuito

![circuito](imagens/falstad_circuito.png "Circuito Falstad")

Link para o [circuito](https://www.falstad.com/circuit/circuitjs.html?ctz=CQAgjCAMB0l3BWEBmWAWAbAdgwDgxmAEwJGQJoq4hJrI0CmAtGGAFABuILpk3RaPjzLdWRKOBAY+YXH3lRoCNgBVRvfoNHI0lFsQmUYYQbjQJIWSGiIlkpcU2MIryaQE40YLBdlh6uNBYuMjkAlg2kMi4CBjM1DDuSWwAJqJkQt4YomCk-AIg4ikMAGYAhgCuADYALqk51qI4OXlMtpRFpZW19fqtxNQsGU3ZneXVdWl9jgMNelmFIMXjPQDuLY6h87mO7VBs6yw62zNgjfrikAdzIzcX+4esrcgzcIMvD+lwmpnDLM1XR7HH5fIQCeTXabcLaiN7QxpENgAY1hkHe5zOaOh4kusDg2CShKJxMKaGguV0vDgskg7iiimpkKeMyIg2Z2P2ACduDh0XpefkjIUMmwdCAAF4MAB2DE5RyYSAgYCCJnsrLA7nV8CIzCwEiIePgRuNZnA9HY4p5eA5TAFbQK4klMrl-gVkLteyY0kGe2Q7pETA+XqxgcubG5gbct0j2VDEmIgO4uDk8P51sDjUTTGTQiDdo+V28em9gu4JZY0kkFm+MEQIAASgwAM4ASybNTKUqRDEhCE1on7CoMLH7iMO5aDCGHBbYNW4U5mg68sY1l0k+mgAjQ7gwfbAGHcZlp9BgWBMISnISIoSn2UxSy6E3d6ZhTGXqc+b-3-BD74zRl7adHD-GcIwnRxy33BRbDgSE-wWGNbj9Q4QM2KM4yzfNHCwANQKTFMhkGHMOT4GDCwwRxiP-fDfmoMZuhqJgqgYNIIAUWt2EeOFqOzAiYU41F3koviwwAcxom1iILfYAHtwHcKQhUEJJCnkTd9XklA2Dk-xFMMWkFK0DBoEoCBxF0v0dOyDAlIM1SGSiYgkBxcBskswokBsqQQGU6gtGMFBwG08BxC8vhlMMvhjMoDoQq0uT7D08K7P89TnJQJA-SAA)

## Projeto do Esquemático 

![esquema](imagens/schema.png "Esquemático no Eagle")

## PCB 

![pcb](imagens/pcb.png "PCB no Eagle")

## Foto da fonte

<p align="center">
  <img src="imagens/fonte.jpg" alt="Fonte" title="Fonte" width="800"/>
</p>

[Link para o vídeo](https://youtu.be/bzNdUfgyO8s)


## Considerações Finais

O curso de Eletrônica para Computação foi uma experiência enriquecedora, abordando componentes essenciais como resistores, capacitores, diodos, diodos Zener e potenciômetros. Aprendemos como cada um desses componentes funciona e suas aplicações práticas em circuitos eletrônicos.

A construção de uma fonte de alimentação foi um projeto que permitiu a aplicação prática de todo o conhecimento adquirido. Utilizamos resistores para controlar a corrente, capacitores para estabilizar e filtrar a tensão, diodos para retificação e diodos Zener para regulação da tensão de saída. O potenciômetro foi essencial para ajustar a tensão de saída da fonte.
